# Ensi\BuClient\StoreWorkingsApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createStoreWorking**](StoreWorkingsApi.md#createStoreWorking) | **POST** /stores/workings | Добавить новую запись о времени работы склада
[**deleteStoreWorking**](StoreWorkingsApi.md#deleteStoreWorking) | **DELETE** /stores/workings/{id} | Удалить запись о времени работы склада
[**extendedPatchStoreWorking**](StoreWorkingsApi.md#extendedPatchStoreWorking) | **PATCH** /stores/workings/{id}:extended | Частично обновить запись о времени работы склада с дополнительной фильтрацией
[**getStoreWorking**](StoreWorkingsApi.md#getStoreWorking) | **GET** /stores/workings/{id} | Получить запись о времени работы склада
[**patchStoreWorking**](StoreWorkingsApi.md#patchStoreWorking) | **PATCH** /stores/workings/{id} | Частично обновить запись о времени работы склада
[**searchStoreWorkings**](StoreWorkingsApi.md#searchStoreWorkings) | **POST** /stores/workings:search | Получить список записей о времени работы склада



## createStoreWorking

> \Ensi\BuClient\Dto\StoreWorkingResponse createStoreWorking($create_store_working_request)

Добавить новую запись о времени работы склада

Создание объекта типа StoreWorking

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BuClient\Api\StoreWorkingsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$create_store_working_request = new \Ensi\BuClient\Dto\CreateStoreWorkingRequest(); // \Ensi\BuClient\Dto\CreateStoreWorkingRequest | 

try {
    $result = $apiInstance->createStoreWorking($create_store_working_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StoreWorkingsApi->createStoreWorking: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_store_working_request** | [**\Ensi\BuClient\Dto\CreateStoreWorkingRequest**](../Model/CreateStoreWorkingRequest.md)|  | [optional]

### Return type

[**\Ensi\BuClient\Dto\StoreWorkingResponse**](../Model/StoreWorkingResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteStoreWorking

> \Ensi\BuClient\Dto\EmptyDataResponse deleteStoreWorking($id, $filter_store_workings_request)

Удалить запись о времени работы склада

Удаление объекта типа StoreWorking

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BuClient\Api\StoreWorkingsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$filter_store_workings_request = new \Ensi\BuClient\Dto\FilterStoreWorkingsRequest(); // \Ensi\BuClient\Dto\FilterStoreWorkingsRequest | 

try {
    $result = $apiInstance->deleteStoreWorking($id, $filter_store_workings_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StoreWorkingsApi->deleteStoreWorking: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **filter_store_workings_request** | [**\Ensi\BuClient\Dto\FilterStoreWorkingsRequest**](../Model/FilterStoreWorkingsRequest.md)|  | [optional]

### Return type

[**\Ensi\BuClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## extendedPatchStoreWorking

> \Ensi\BuClient\Dto\StoreWorkingResponse extendedPatchStoreWorking($id, $extended_patch_store_working_request)

Частично обновить запись о времени работы склада с дополнительной фильтрацией

Обновления части полей объекта типа StoreWorking с дополнительной фильтрацией

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BuClient\Api\StoreWorkingsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$extended_patch_store_working_request = new \Ensi\BuClient\Dto\ExtendedPatchStoreWorkingRequest(); // \Ensi\BuClient\Dto\ExtendedPatchStoreWorkingRequest | 

try {
    $result = $apiInstance->extendedPatchStoreWorking($id, $extended_patch_store_working_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StoreWorkingsApi->extendedPatchStoreWorking: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **extended_patch_store_working_request** | [**\Ensi\BuClient\Dto\ExtendedPatchStoreWorkingRequest**](../Model/ExtendedPatchStoreWorkingRequest.md)|  |

### Return type

[**\Ensi\BuClient\Dto\StoreWorkingResponse**](../Model/StoreWorkingResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getStoreWorking

> \Ensi\BuClient\Dto\StoreWorkingResponse getStoreWorking($id, $include)

Получить запись о времени работы склада

Получение объекта типа StoreWorking

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BuClient\Api\StoreWorkingsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$include = 'include_example'; // string | Связанные сущности для подгрузки, через запятую

try {
    $result = $apiInstance->getStoreWorking($id, $include);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StoreWorkingsApi->getStoreWorking: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **include** | **string**| Связанные сущности для подгрузки, через запятую | [optional]

### Return type

[**\Ensi\BuClient\Dto\StoreWorkingResponse**](../Model/StoreWorkingResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchStoreWorking

> \Ensi\BuClient\Dto\StoreWorkingResponse patchStoreWorking($id, $patch_store_working_request)

Частично обновить запись о времени работы склада

Обновления части полей объекта типа StoreWorking

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BuClient\Api\StoreWorkingsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$patch_store_working_request = new \Ensi\BuClient\Dto\PatchStoreWorkingRequest(); // \Ensi\BuClient\Dto\PatchStoreWorkingRequest | 

try {
    $result = $apiInstance->patchStoreWorking($id, $patch_store_working_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StoreWorkingsApi->patchStoreWorking: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **patch_store_working_request** | [**\Ensi\BuClient\Dto\PatchStoreWorkingRequest**](../Model/PatchStoreWorkingRequest.md)|  |

### Return type

[**\Ensi\BuClient\Dto\StoreWorkingResponse**](../Model/StoreWorkingResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchStoreWorkings

> \Ensi\BuClient\Dto\SearchStoreWorkingsResponse searchStoreWorkings($search_store_workings_request)

Получить список записей о времени работы склада

Поиск объектов типа StoreWorking

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BuClient\Api\StoreWorkingsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_store_workings_request = new \Ensi\BuClient\Dto\SearchStoreWorkingsRequest(); // \Ensi\BuClient\Dto\SearchStoreWorkingsRequest | 

try {
    $result = $apiInstance->searchStoreWorkings($search_store_workings_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StoreWorkingsApi->searchStoreWorkings: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_store_workings_request** | [**\Ensi\BuClient\Dto\SearchStoreWorkingsRequest**](../Model/SearchStoreWorkingsRequest.md)|  |

### Return type

[**\Ensi\BuClient\Dto\SearchStoreWorkingsResponse**](../Model/SearchStoreWorkingsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

