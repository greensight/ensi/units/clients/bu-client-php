# Ensi\BuClient\SellersApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createSeller**](SellersApi.md#createSeller) | **POST** /sellers/sellers | Добавить нового продавца
[**createSellerWithOperator**](SellersApi.md#createSellerWithOperator) | **POST** /sellers/sellers:create | Добавить нового продавца с оператором
[**deleteSeller**](SellersApi.md#deleteSeller) | **DELETE** /sellers/sellers/{id} | Удаление продавца
[**getSeller**](SellersApi.md#getSeller) | **GET** /sellers/sellers/{id} | Получить информацию о продавце
[**patchSeller**](SellersApi.md#patchSeller) | **PATCH** /sellers/sellers/{id} | Частично обновить информацию о продавце
[**registerSellerWithOperator**](SellersApi.md#registerSellerWithOperator) | **POST** /sellers/sellers:register | Зарегистрировать нового продавца
[**searchSellers**](SellersApi.md#searchSellers) | **POST** /sellers/sellers:search | Получить список продавецов, удовлетворяющих условиям



## createSeller

> \Ensi\BuClient\Dto\SellerResponse createSeller($create_seller_request)

Добавить нового продавца

Создание объекта типа Seller

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BuClient\Api\SellersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$create_seller_request = new \Ensi\BuClient\Dto\CreateSellerRequest(); // \Ensi\BuClient\Dto\CreateSellerRequest | 

try {
    $result = $apiInstance->createSeller($create_seller_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SellersApi->createSeller: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_seller_request** | [**\Ensi\BuClient\Dto\CreateSellerRequest**](../Model/CreateSellerRequest.md)|  |

### Return type

[**\Ensi\BuClient\Dto\SellerResponse**](../Model/SellerResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## createSellerWithOperator

> \Ensi\BuClient\Dto\SellerResponse createSellerWithOperator($create_with_operator_seller_request)

Добавить нового продавца с оператором

Создание объекта типа Seller со статусом \"Одобрен\", объекта Operator (также под капотом создается объект User в мс Admin-Auth)

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BuClient\Api\SellersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$create_with_operator_seller_request = new \Ensi\BuClient\Dto\CreateWithOperatorSellerRequest(); // \Ensi\BuClient\Dto\CreateWithOperatorSellerRequest | 

try {
    $result = $apiInstance->createSellerWithOperator($create_with_operator_seller_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SellersApi->createSellerWithOperator: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_with_operator_seller_request** | [**\Ensi\BuClient\Dto\CreateWithOperatorSellerRequest**](../Model/CreateWithOperatorSellerRequest.md)|  |

### Return type

[**\Ensi\BuClient\Dto\SellerResponse**](../Model/SellerResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteSeller

> \Ensi\BuClient\Dto\EmptyDataResponse deleteSeller($id)

Удаление продавца

Удаление объекта типа Seller

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BuClient\Api\SellersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->deleteSeller($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SellersApi->deleteSeller: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\BuClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getSeller

> \Ensi\BuClient\Dto\SellerResponse getSeller($id, $include)

Получить информацию о продавце

Получение объекта типа Seller

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BuClient\Api\SellersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$include = 'include_example'; // string | Связанные сущности для подгрузки, через запятую

try {
    $result = $apiInstance->getSeller($id, $include);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SellersApi->getSeller: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **include** | **string**| Связанные сущности для подгрузки, через запятую | [optional]

### Return type

[**\Ensi\BuClient\Dto\SellerResponse**](../Model/SellerResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchSeller

> \Ensi\BuClient\Dto\SellerResponse patchSeller($id, $patch_seller_request)

Частично обновить информацию о продавце

Обновление части полей объекта типа Seller

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BuClient\Api\SellersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$patch_seller_request = new \Ensi\BuClient\Dto\PatchSellerRequest(); // \Ensi\BuClient\Dto\PatchSellerRequest | 

try {
    $result = $apiInstance->patchSeller($id, $patch_seller_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SellersApi->patchSeller: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **patch_seller_request** | [**\Ensi\BuClient\Dto\PatchSellerRequest**](../Model/PatchSellerRequest.md)|  |

### Return type

[**\Ensi\BuClient\Dto\SellerResponse**](../Model/SellerResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## registerSellerWithOperator

> \Ensi\BuClient\Dto\SellerResponse registerSellerWithOperator($register_seller_request)

Зарегистрировать нового продавца

Создание объекта типа Seller со статусом \"Только создан, информации нет\", объекта Operator (также под капотом создается объект User в мс Admin-Auth)

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BuClient\Api\SellersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$register_seller_request = new \Ensi\BuClient\Dto\RegisterSellerRequest(); // \Ensi\BuClient\Dto\RegisterSellerRequest | 

try {
    $result = $apiInstance->registerSellerWithOperator($register_seller_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SellersApi->registerSellerWithOperator: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **register_seller_request** | [**\Ensi\BuClient\Dto\RegisterSellerRequest**](../Model/RegisterSellerRequest.md)|  |

### Return type

[**\Ensi\BuClient\Dto\SellerResponse**](../Model/SellerResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchSellers

> \Ensi\BuClient\Dto\SearchSellersResponse searchSellers($search_sellers_request)

Получить список продавецов, удовлетворяющих условиям

Поиск объектов типа Seller

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BuClient\Api\SellersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_sellers_request = new \Ensi\BuClient\Dto\SearchSellersRequest(); // \Ensi\BuClient\Dto\SearchSellersRequest | 

try {
    $result = $apiInstance->searchSellers($search_sellers_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SellersApi->searchSellers: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_sellers_request** | [**\Ensi\BuClient\Dto\SearchSellersRequest**](../Model/SearchSellersRequest.md)|  |

### Return type

[**\Ensi\BuClient\Dto\SearchSellersResponse**](../Model/SearchSellersResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

