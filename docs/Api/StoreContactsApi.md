# Ensi\BuClient\StoreContactsApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createStoreContact**](StoreContactsApi.md#createStoreContact) | **POST** /stores/contacts | Добавить новое контактное лицо склада
[**deleteStoreContact**](StoreContactsApi.md#deleteStoreContact) | **DELETE** /stores/contacts/{id} | Удалить информацию о контактном лице склада
[**extendedPatchStoreContact**](StoreContactsApi.md#extendedPatchStoreContact) | **PATCH** /stores/contacts/{id}:extended | Частично обновить информацию о контактном лице  склада с дополнительной фильтрацией
[**getStoreContact**](StoreContactsApi.md#getStoreContact) | **GET** /stores/contacts/{id} | Получить информацию о контактном лице склада
[**patchStoreContact**](StoreContactsApi.md#patchStoreContact) | **PATCH** /stores/contacts/{id} | Частично обновить информацию о контактном лице  склада
[**searchStoreContacts**](StoreContactsApi.md#searchStoreContacts) | **POST** /stores/contacts:search | Получить список контактных лиц склада, удовлетворяющих условиям



## createStoreContact

> \Ensi\BuClient\Dto\StoreContactResponse createStoreContact($create_store_contact_request)

Добавить новое контактное лицо склада

Создание объекта типа StoreContact

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BuClient\Api\StoreContactsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$create_store_contact_request = new \Ensi\BuClient\Dto\CreateStoreContactRequest(); // \Ensi\BuClient\Dto\CreateStoreContactRequest | 

try {
    $result = $apiInstance->createStoreContact($create_store_contact_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StoreContactsApi->createStoreContact: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_store_contact_request** | [**\Ensi\BuClient\Dto\CreateStoreContactRequest**](../Model/CreateStoreContactRequest.md)|  | [optional]

### Return type

[**\Ensi\BuClient\Dto\StoreContactResponse**](../Model/StoreContactResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteStoreContact

> \Ensi\BuClient\Dto\EmptyDataResponse deleteStoreContact($id, $filter_store_contacts_request)

Удалить информацию о контактном лице склада

Удаление объекта типа StoreContact

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BuClient\Api\StoreContactsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$filter_store_contacts_request = new \Ensi\BuClient\Dto\FilterStoreContactsRequest(); // \Ensi\BuClient\Dto\FilterStoreContactsRequest | 

try {
    $result = $apiInstance->deleteStoreContact($id, $filter_store_contacts_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StoreContactsApi->deleteStoreContact: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **filter_store_contacts_request** | [**\Ensi\BuClient\Dto\FilterStoreContactsRequest**](../Model/FilterStoreContactsRequest.md)|  | [optional]

### Return type

[**\Ensi\BuClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## extendedPatchStoreContact

> \Ensi\BuClient\Dto\StoreContactResponse extendedPatchStoreContact($id, $extended_patch_store_contact_request)

Частично обновить информацию о контактном лице  склада с дополнительной фильтрацией

Обновления части полей объекта типа StoreContact с дополнительной фильтрацией

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BuClient\Api\StoreContactsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$extended_patch_store_contact_request = new \Ensi\BuClient\Dto\ExtendedPatchStoreContactRequest(); // \Ensi\BuClient\Dto\ExtendedPatchStoreContactRequest | 

try {
    $result = $apiInstance->extendedPatchStoreContact($id, $extended_patch_store_contact_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StoreContactsApi->extendedPatchStoreContact: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **extended_patch_store_contact_request** | [**\Ensi\BuClient\Dto\ExtendedPatchStoreContactRequest**](../Model/ExtendedPatchStoreContactRequest.md)|  |

### Return type

[**\Ensi\BuClient\Dto\StoreContactResponse**](../Model/StoreContactResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getStoreContact

> \Ensi\BuClient\Dto\StoreContactResponse getStoreContact($id, $include)

Получить информацию о контактном лице склада

Получение объекта типа StoreContact

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BuClient\Api\StoreContactsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$include = 'include_example'; // string | Связанные сущности для подгрузки, через запятую

try {
    $result = $apiInstance->getStoreContact($id, $include);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StoreContactsApi->getStoreContact: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **include** | **string**| Связанные сущности для подгрузки, через запятую | [optional]

### Return type

[**\Ensi\BuClient\Dto\StoreContactResponse**](../Model/StoreContactResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchStoreContact

> \Ensi\BuClient\Dto\StoreContactResponse patchStoreContact($id, $patch_store_contact_request)

Частично обновить информацию о контактном лице  склада

Обновления части полей объекта типа StoreContact

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BuClient\Api\StoreContactsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$patch_store_contact_request = new \Ensi\BuClient\Dto\PatchStoreContactRequest(); // \Ensi\BuClient\Dto\PatchStoreContactRequest | 

try {
    $result = $apiInstance->patchStoreContact($id, $patch_store_contact_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StoreContactsApi->patchStoreContact: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **patch_store_contact_request** | [**\Ensi\BuClient\Dto\PatchStoreContactRequest**](../Model/PatchStoreContactRequest.md)|  |

### Return type

[**\Ensi\BuClient\Dto\StoreContactResponse**](../Model/StoreContactResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchStoreContacts

> \Ensi\BuClient\Dto\SearchStoreContactsResponse searchStoreContacts($search_store_contacts_request)

Получить список контактных лиц склада, удовлетворяющих условиям

Поиск объектов типа StoreContact

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BuClient\Api\StoreContactsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_store_contacts_request = new \Ensi\BuClient\Dto\SearchStoreContactsRequest(); // \Ensi\BuClient\Dto\SearchStoreContactsRequest | 

try {
    $result = $apiInstance->searchStoreContacts($search_store_contacts_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StoreContactsApi->searchStoreContacts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_store_contacts_request** | [**\Ensi\BuClient\Dto\SearchStoreContactsRequest**](../Model/SearchStoreContactsRequest.md)|  |

### Return type

[**\Ensi\BuClient\Dto\SearchStoreContactsResponse**](../Model/SearchStoreContactsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

