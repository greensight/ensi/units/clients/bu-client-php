# Ensi\BuClient\StorePickupTimesApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createStorePickupTime**](StorePickupTimesApi.md#createStorePickupTime) | **POST** /stores/pickup-times | Добавить новую запись о времени отгрузки со склада
[**deleteStorePickupTime**](StorePickupTimesApi.md#deleteStorePickupTime) | **DELETE** /stores/pickup-times/{id} | Удалить запись о времени отгрузки со склада
[**extendedPatchStorePickupTime**](StorePickupTimesApi.md#extendedPatchStorePickupTime) | **PATCH** /stores/pickup-times/{id}:extended | Частично обновить запись о времени отгрузки со склада с дополнительной фильтрацией
[**getStorePickupTime**](StorePickupTimesApi.md#getStorePickupTime) | **GET** /stores/pickup-times/{id} | Получить запись о времени отгрузки со склада
[**patchStorePickupTime**](StorePickupTimesApi.md#patchStorePickupTime) | **PATCH** /stores/pickup-times/{id} | Частично обновить запись о времени отгрузки со склада
[**searchStorePickupTimes**](StorePickupTimesApi.md#searchStorePickupTimes) | **POST** /stores/pickup-times:search | Получить список записей о времени отгрузки со склада



## createStorePickupTime

> \Ensi\BuClient\Dto\StorePickupTimeResponse createStorePickupTime($create_store_pickup_time_request)

Добавить новую запись о времени отгрузки со склада

Создание объекта типа StorePickupTime

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BuClient\Api\StorePickupTimesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$create_store_pickup_time_request = new \Ensi\BuClient\Dto\CreateStorePickupTimeRequest(); // \Ensi\BuClient\Dto\CreateStorePickupTimeRequest | 

try {
    $result = $apiInstance->createStorePickupTime($create_store_pickup_time_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StorePickupTimesApi->createStorePickupTime: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_store_pickup_time_request** | [**\Ensi\BuClient\Dto\CreateStorePickupTimeRequest**](../Model/CreateStorePickupTimeRequest.md)|  | [optional]

### Return type

[**\Ensi\BuClient\Dto\StorePickupTimeResponse**](../Model/StorePickupTimeResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteStorePickupTime

> \Ensi\BuClient\Dto\EmptyDataResponse deleteStorePickupTime($id, $filter_store_pickup_times_request)

Удалить запись о времени отгрузки со склада

Удаление объекта типа StorePickupTime

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BuClient\Api\StorePickupTimesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$filter_store_pickup_times_request = new \Ensi\BuClient\Dto\FilterStorePickupTimesRequest(); // \Ensi\BuClient\Dto\FilterStorePickupTimesRequest | 

try {
    $result = $apiInstance->deleteStorePickupTime($id, $filter_store_pickup_times_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StorePickupTimesApi->deleteStorePickupTime: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **filter_store_pickup_times_request** | [**\Ensi\BuClient\Dto\FilterStorePickupTimesRequest**](../Model/FilterStorePickupTimesRequest.md)|  | [optional]

### Return type

[**\Ensi\BuClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## extendedPatchStorePickupTime

> \Ensi\BuClient\Dto\StorePickupTimeResponse extendedPatchStorePickupTime($id, $extended_patch_store_pickup_time_request)

Частично обновить запись о времени отгрузки со склада с дополнительной фильтрацией

Обновления части полей объекта типа StorePickupTime с дополнительной фильтрацией

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BuClient\Api\StorePickupTimesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$extended_patch_store_pickup_time_request = new \Ensi\BuClient\Dto\ExtendedPatchStorePickupTimeRequest(); // \Ensi\BuClient\Dto\ExtendedPatchStorePickupTimeRequest | 

try {
    $result = $apiInstance->extendedPatchStorePickupTime($id, $extended_patch_store_pickup_time_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StorePickupTimesApi->extendedPatchStorePickupTime: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **extended_patch_store_pickup_time_request** | [**\Ensi\BuClient\Dto\ExtendedPatchStorePickupTimeRequest**](../Model/ExtendedPatchStorePickupTimeRequest.md)|  |

### Return type

[**\Ensi\BuClient\Dto\StorePickupTimeResponse**](../Model/StorePickupTimeResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getStorePickupTime

> \Ensi\BuClient\Dto\StorePickupTimeResponse getStorePickupTime($id, $include)

Получить запись о времени отгрузки со склада

Получение объекта типа StorePickupTime

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BuClient\Api\StorePickupTimesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$include = 'include_example'; // string | Связанные сущности для подгрузки, через запятую

try {
    $result = $apiInstance->getStorePickupTime($id, $include);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StorePickupTimesApi->getStorePickupTime: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **include** | **string**| Связанные сущности для подгрузки, через запятую | [optional]

### Return type

[**\Ensi\BuClient\Dto\StorePickupTimeResponse**](../Model/StorePickupTimeResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchStorePickupTime

> \Ensi\BuClient\Dto\StorePickupTimeResponse patchStorePickupTime($id, $patch_store_pickup_time_request)

Частично обновить запись о времени отгрузки со склада

Обновления части полей объекта типа StorePickupTime

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BuClient\Api\StorePickupTimesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$patch_store_pickup_time_request = new \Ensi\BuClient\Dto\PatchStorePickupTimeRequest(); // \Ensi\BuClient\Dto\PatchStorePickupTimeRequest | 

try {
    $result = $apiInstance->patchStorePickupTime($id, $patch_store_pickup_time_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StorePickupTimesApi->patchStorePickupTime: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **patch_store_pickup_time_request** | [**\Ensi\BuClient\Dto\PatchStorePickupTimeRequest**](../Model/PatchStorePickupTimeRequest.md)|  |

### Return type

[**\Ensi\BuClient\Dto\StorePickupTimeResponse**](../Model/StorePickupTimeResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchStorePickupTimes

> \Ensi\BuClient\Dto\SearchStorePickupTimesResponse searchStorePickupTimes($search_store_pickup_times_request)

Получить список записей о времени отгрузки со склада

Поиск объектов типа StorePickupTime

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BuClient\Api\StorePickupTimesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_store_pickup_times_request = new \Ensi\BuClient\Dto\SearchStorePickupTimesRequest(); // \Ensi\BuClient\Dto\SearchStorePickupTimesRequest | 

try {
    $result = $apiInstance->searchStorePickupTimes($search_store_pickup_times_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StorePickupTimesApi->searchStorePickupTimes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_store_pickup_times_request** | [**\Ensi\BuClient\Dto\SearchStorePickupTimesRequest**](../Model/SearchStorePickupTimesRequest.md)|  |

### Return type

[**\Ensi\BuClient\Dto\SearchStorePickupTimesResponse**](../Model/SearchStorePickupTimesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

