# Ensi\BuClient\OperatorsApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createOperator**](OperatorsApi.md#createOperator) | **POST** /seller-users/operators | Добавить нового оператора
[**deleteOperator**](OperatorsApi.md#deleteOperator) | **DELETE** /seller-users/operators/{id} | Удаление оператора
[**getOperator**](OperatorsApi.md#getOperator) | **GET** /seller-users/operators/{id} | Получить информацию об операторе
[**patchOperator**](OperatorsApi.md#patchOperator) | **PATCH** /seller-users/operators/{id} | Частично обновить информацию об операторе
[**searchOperators**](OperatorsApi.md#searchOperators) | **POST** /seller-users/operators:search | Получить список операторов, удовлетворяющих условиям



## createOperator

> \Ensi\BuClient\Dto\OperatorResponse createOperator($create_operator_request)

Добавить нового оператора

Создание объекта типа Operator

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BuClient\Api\OperatorsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$create_operator_request = new \Ensi\BuClient\Dto\CreateOperatorRequest(); // \Ensi\BuClient\Dto\CreateOperatorRequest | 

try {
    $result = $apiInstance->createOperator($create_operator_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OperatorsApi->createOperator: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_operator_request** | [**\Ensi\BuClient\Dto\CreateOperatorRequest**](../Model/CreateOperatorRequest.md)|  |

### Return type

[**\Ensi\BuClient\Dto\OperatorResponse**](../Model/OperatorResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteOperator

> \Ensi\BuClient\Dto\EmptyDataResponse deleteOperator($id)

Удаление оператора

Удаление объекта типа Operator

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BuClient\Api\OperatorsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->deleteOperator($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OperatorsApi->deleteOperator: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\BuClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getOperator

> \Ensi\BuClient\Dto\OperatorResponse getOperator($id, $include)

Получить информацию об операторе

Получение объекта типа Operator

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BuClient\Api\OperatorsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$include = 'include_example'; // string | Связанные сущности для подгрузки, через запятую

try {
    $result = $apiInstance->getOperator($id, $include);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OperatorsApi->getOperator: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **include** | **string**| Связанные сущности для подгрузки, через запятую | [optional]

### Return type

[**\Ensi\BuClient\Dto\OperatorResponse**](../Model/OperatorResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchOperator

> \Ensi\BuClient\Dto\OperatorResponse patchOperator($id, $patch_operator_request)

Частично обновить информацию об операторе

Обновление части полей объекта типа Operator

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BuClient\Api\OperatorsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$patch_operator_request = new \Ensi\BuClient\Dto\PatchOperatorRequest(); // \Ensi\BuClient\Dto\PatchOperatorRequest | 

try {
    $result = $apiInstance->patchOperator($id, $patch_operator_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OperatorsApi->patchOperator: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **patch_operator_request** | [**\Ensi\BuClient\Dto\PatchOperatorRequest**](../Model/PatchOperatorRequest.md)|  |

### Return type

[**\Ensi\BuClient\Dto\OperatorResponse**](../Model/OperatorResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchOperators

> \Ensi\BuClient\Dto\SearchOperatorsResponse searchOperators($search_operators_request)

Получить список операторов, удовлетворяющих условиям

Поиск объектов типа Operator

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BuClient\Api\OperatorsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_operators_request = new \Ensi\BuClient\Dto\SearchOperatorsRequest(); // \Ensi\BuClient\Dto\SearchOperatorsRequest | 

try {
    $result = $apiInstance->searchOperators($search_operators_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OperatorsApi->searchOperators: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_operators_request** | [**\Ensi\BuClient\Dto\SearchOperatorsRequest**](../Model/SearchOperatorsRequest.md)|  |

### Return type

[**\Ensi\BuClient\Dto\SearchOperatorsResponse**](../Model/SearchOperatorsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

