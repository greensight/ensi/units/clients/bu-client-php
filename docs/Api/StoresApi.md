# Ensi\BuClient\StoresApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createStore**](StoresApi.md#createStore) | **POST** /stores/stores | Добавить новый склад
[**extendedPatchStore**](StoresApi.md#extendedPatchStore) | **PATCH** /stores/stores/{id}:extended | Частично обновить информацию о складе с дополнительной фильтрацией
[**getStore**](StoresApi.md#getStore) | **GET** /stores/stores/{id} | Получить информацию о складе
[**patchStore**](StoresApi.md#patchStore) | **PATCH** /stores/stores/{id} | Частично обновить информацию о складе
[**searchOneStore**](StoresApi.md#searchOneStore) | **POST** /stores/stores:search-one | Поиск склада, удовлетворяющего условиям
[**searchStores**](StoresApi.md#searchStores) | **POST** /stores/stores:search | Получить список складов, удовлетворяющих условиям



## createStore

> \Ensi\BuClient\Dto\StoreResponse createStore($create_store_request)

Добавить новый склад

Создание объекта типа Store

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BuClient\Api\StoresApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$create_store_request = new \Ensi\BuClient\Dto\CreateStoreRequest(); // \Ensi\BuClient\Dto\CreateStoreRequest | 

try {
    $result = $apiInstance->createStore($create_store_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StoresApi->createStore: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_store_request** | [**\Ensi\BuClient\Dto\CreateStoreRequest**](../Model/CreateStoreRequest.md)|  | [optional]

### Return type

[**\Ensi\BuClient\Dto\StoreResponse**](../Model/StoreResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## extendedPatchStore

> \Ensi\BuClient\Dto\StoreResponse extendedPatchStore($id, $extended_patch_store_request)

Частично обновить информацию о складе с дополнительной фильтрацией

Обновления части полей объекта типа Store с дополнительной фильтрацией

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BuClient\Api\StoresApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$extended_patch_store_request = new \Ensi\BuClient\Dto\ExtendedPatchStoreRequest(); // \Ensi\BuClient\Dto\ExtendedPatchStoreRequest | 

try {
    $result = $apiInstance->extendedPatchStore($id, $extended_patch_store_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StoresApi->extendedPatchStore: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **extended_patch_store_request** | [**\Ensi\BuClient\Dto\ExtendedPatchStoreRequest**](../Model/ExtendedPatchStoreRequest.md)|  |

### Return type

[**\Ensi\BuClient\Dto\StoreResponse**](../Model/StoreResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getStore

> \Ensi\BuClient\Dto\StoreResponse getStore($id, $include)

Получить информацию о складе

Получение объекта типа Store

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BuClient\Api\StoresApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$include = 'include_example'; // string | Связанные сущности для подгрузки, через запятую

try {
    $result = $apiInstance->getStore($id, $include);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StoresApi->getStore: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **include** | **string**| Связанные сущности для подгрузки, через запятую | [optional]

### Return type

[**\Ensi\BuClient\Dto\StoreResponse**](../Model/StoreResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchStore

> \Ensi\BuClient\Dto\StoreResponse patchStore($id, $patch_store_request)

Частично обновить информацию о складе

Обновления части полей объекта типа Store

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BuClient\Api\StoresApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$patch_store_request = new \Ensi\BuClient\Dto\PatchStoreRequest(); // \Ensi\BuClient\Dto\PatchStoreRequest | 

try {
    $result = $apiInstance->patchStore($id, $patch_store_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StoresApi->patchStore: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **patch_store_request** | [**\Ensi\BuClient\Dto\PatchStoreRequest**](../Model/PatchStoreRequest.md)|  |

### Return type

[**\Ensi\BuClient\Dto\StoreResponse**](../Model/StoreResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchOneStore

> \Ensi\BuClient\Dto\StoreResponse searchOneStore($search_stores_request)

Поиск склада, удовлетворяющего условиям

Поиск объектов типа Store

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BuClient\Api\StoresApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_stores_request = new \Ensi\BuClient\Dto\SearchStoresRequest(); // \Ensi\BuClient\Dto\SearchStoresRequest | 

try {
    $result = $apiInstance->searchOneStore($search_stores_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StoresApi->searchOneStore: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_stores_request** | [**\Ensi\BuClient\Dto\SearchStoresRequest**](../Model/SearchStoresRequest.md)|  |

### Return type

[**\Ensi\BuClient\Dto\StoreResponse**](../Model/StoreResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchStores

> \Ensi\BuClient\Dto\SearchStoresResponse searchStores($search_stores_request)

Получить список складов, удовлетворяющих условиям

Поиск объектов типа Store

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BuClient\Api\StoresApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_stores_request = new \Ensi\BuClient\Dto\SearchStoresRequest(); // \Ensi\BuClient\Dto\SearchStoresRequest | 

try {
    $result = $apiInstance->searchStores($search_stores_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StoresApi->searchStores: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_stores_request** | [**\Ensi\BuClient\Dto\SearchStoresRequest**](../Model/SearchStoresRequest.md)|  |

### Return type

[**\Ensi\BuClient\Dto\SearchStoresResponse**](../Model/SearchStoresResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

