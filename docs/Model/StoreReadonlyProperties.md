# # StoreReadonlyProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор склада | 
**created_at** | [**\DateTime**](\DateTime.md) | Время создания склада | 
**updated_at** | [**\DateTime**](\DateTime.md) | Время обновления склада | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


