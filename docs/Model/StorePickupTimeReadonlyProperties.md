# # StorePickupTimeReadonlyProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор записи о времени отгрузки | 
**created_at** | [**\DateTime**](\DateTime.md) | Время создания записи о времени отгрузки | 
**updated_at** | [**\DateTime**](\DateTime.md) | Время обновления записи о времени отгрузки | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


