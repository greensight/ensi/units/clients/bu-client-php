# # OperatorIncludes

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**seller** | [**\Ensi\BuClient\Dto\Seller**](Seller.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


