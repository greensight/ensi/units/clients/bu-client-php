# # OperatorReadonlyProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор оператора | 
**created_at** | [**\DateTime**](\DateTime.md) | Время создания оператора | 
**updated_at** | [**\DateTime**](\DateTime.md) | Время обновления оператора | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


