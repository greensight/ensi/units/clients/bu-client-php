# # SearchStoreWorkingsResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**\Ensi\BuClient\Dto\StoreWorking[]**](StoreWorking.md) |  | 
**meta** | [**\Ensi\BuClient\Dto\SearchSellersResponseMeta**](SearchSellersResponseMeta.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


