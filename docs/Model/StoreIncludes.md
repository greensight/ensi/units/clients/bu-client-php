# # StoreIncludes

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**workings** | [**\Ensi\BuClient\Dto\StoreWorking[]**](StoreWorking.md) |  | [optional] 
**contacts** | [**\Ensi\BuClient\Dto\StoreContact[]**](StoreContact.md) |  | [optional] 
**contact** | [**\Ensi\BuClient\Dto\StoreContact**](StoreContact.md) |  | [optional] 
**pickup_times** | [**\Ensi\BuClient\Dto\StorePickupTime[]**](StorePickupTime.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


