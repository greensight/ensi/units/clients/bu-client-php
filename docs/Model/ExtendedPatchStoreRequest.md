# # ExtendedPatchStoreRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fields** | [**\Ensi\BuClient\Dto\PatchStoreRequest**](PatchStoreRequest.md) |  | [optional] 
**filter** | [**object**](.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


