# # StorePickupTime

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор записи о времени отгрузки | 
**created_at** | [**\DateTime**](\DateTime.md) | Время создания записи о времени отгрузки | 
**updated_at** | [**\DateTime**](\DateTime.md) | Время обновления записи о времени отгрузки | 
**store_id** | **int** | ID склада | [optional] 
**day** | **int** | День недели (1-7) | [optional] 
**pickup_time_code** | **string** | Код времени отгрузки у службы доставки | [optional] 
**pickup_time_start** | **string** | Время начала отгрузки | [optional] 
**pickup_time_end** | **string** | Время окончания отгрузки | [optional] 
**delivery_service** | **int** | Служба доставки (если указана, то данная информация переопределяет данные дня недели без службы доставки) | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


