# # Seller

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор продавца | 
**status_at** | [**\DateTime**](\DateTime.md) | Время изменения статуса продавца | 
**created_at** | [**\DateTime**](\DateTime.md) | Время создания продавца | 
**updated_at** | [**\DateTime**](\DateTime.md) | Время обновления продавца | 
**manager_id** | **int** | ID менеджера | [optional] 
**legal_name** | **string** | Юридическое наименование организации | [optional] 
**legal_address** | [**\Ensi\BuClient\Dto\Address**](Address.md) |  | [optional] 
**fact_address** | [**\Ensi\BuClient\Dto\Address**](Address.md) |  | [optional] 
**inn** | **string** | ИНН | [optional] 
**kpp** | **string** | КПП | [optional] 
**payment_account** | **string** | Номер банковского счета | [optional] 
**correspondent_account** | **string** | Номер корреспондентского счета банка | [optional] 
**bank** | **string** | Наименование банка | [optional] 
**bank_address** | [**\Ensi\BuClient\Dto\Address**](Address.md) |  | [optional] 
**bank_bik** | **string** | БИК банка | [optional] 
**site** | **string** | Сайт компании | [optional] 
**info** | **string** | Бренды и товары, которыми торгует Продавец | [optional] 
**status** | **int** | Статус продавца из SellerStatusEnum | [optional] 
**operators** | [**\Ensi\BuClient\Dto\Operator[]**](Operator.md) |  | [optional] 
**stores** | [**\Ensi\BuClient\Dto\Store[]**](Store.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


