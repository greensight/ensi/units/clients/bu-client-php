# # StoreWorkingReadonlyProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор записи о времени работы | 
**created_at** | [**\DateTime**](\DateTime.md) | Время создания записи о времени работы | 
**updated_at** | [**\DateTime**](\DateTime.md) | Время обновления записи о времени работы | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


