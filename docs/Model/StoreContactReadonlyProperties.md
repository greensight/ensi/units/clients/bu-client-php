# # StoreContactReadonlyProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор контактного лица для склада | 
**created_at** | [**\DateTime**](\DateTime.md) | Время создания контактного лица для склада | 
**updated_at** | [**\DateTime**](\DateTime.md) | Время обновления контактного лица для склада | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


