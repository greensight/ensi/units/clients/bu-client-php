# # SellerFillableProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**manager_id** | **int** | ID менеджера | [optional] 
**legal_name** | **string** | Юридическое наименование организации | [optional] 
**legal_address** | [**\Ensi\BuClient\Dto\Address**](Address.md) |  | [optional] 
**fact_address** | [**\Ensi\BuClient\Dto\Address**](Address.md) |  | [optional] 
**inn** | **string** | ИНН | [optional] 
**kpp** | **string** | КПП | [optional] 
**payment_account** | **string** | Номер банковского счета | [optional] 
**correspondent_account** | **string** | Номер корреспондентского счета банка | [optional] 
**bank** | **string** | Наименование банка | [optional] 
**bank_address** | [**\Ensi\BuClient\Dto\Address**](Address.md) |  | [optional] 
**bank_bik** | **string** | БИК банка | [optional] 
**site** | **string** | Сайт компании | [optional] 
**info** | **string** | Бренды и товары, которыми торгует Продавец | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


