# # SellerReadonlyProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор продавца | 
**status_at** | [**\DateTime**](\DateTime.md) | Время изменения статуса продавца | 
**created_at** | [**\DateTime**](\DateTime.md) | Время создания продавца | 
**updated_at** | [**\DateTime**](\DateTime.md) | Время обновления продавца | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


